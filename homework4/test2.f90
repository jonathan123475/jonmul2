
program test1

    use quadrature, only: trapezoid, error_table

    implicit none
    real(kind=8) :: a,b,int_true
    integer :: nvals(7), i

    a = 0.d0
    b = 2.d0
    int_true = (b-a) + (b**4 - a**4) / 4.d0 - (1.d0/1000)*(cos(1000*b)-cos(1000*a))

    print 10, int_true
 10 format("true integral: ", es22.14)
    print *, " "  ! blank line

    ! values of n to test:
    do i=1,12
        nvals(i) = 5 * 2**(i-1)
        enddo
    a = 0.d0
    b = 2.d0
    call error_table(f, a, b, nvals, int_true)

contains

    real(kind=8) function f(x)
        implicit none
        real(kind=8), intent(in) :: x 
	real(kind=8), parameter :: k = 1000
        !$ call omp_set_num_threads(2)
	!$omp parallel do private(xj)
        f = 1.d0 + x**3 + sin(k*x)
    end function f

end program test1
