

module quadrature

	implicit none
	

contains
subroutine trapezoid(f, a, b, n, int_trapezoid)
	
	implicit none
	real(kind=8), intent(in) :: a,b
	real(kind=8), external :: f
	
	integer, intent(in) :: n
	!Define Local Variables
	real(kind=8) :: h
	real(kind=8), dimension(1:n) :: xj, fj
	integer :: k

	real(kind=8), intent(out) :: int_trapezoid
	h = (b-a)/(n-1)
	do k=1, n
		xj(1:k)= a + k*(b-a)/n
		fj(k)= f(xj(k))	
		!print *, xj(k), fj(k)
		!print *, a, b, n
		enddo

	!fj = f(xj)
	!print *, n
	int_trapezoid = h*sum(fj) - 0.5*h*(fj(1) + fj(size(fj)))
	!h*sum(fj) - 0.5*h*(fj(1) + fj(size(fj)))
end subroutine trapezoid


subroutine error_table(f,a,b,nvals,int_true)
	
	implicit none
	real(kind=8) :: int_trap, error, ratio
	real(kind=8) :: last_error = 0
	real(kind=8), intent(in) :: a,b,int_true
	real(kind=8), external :: f
	integer, dimension(1:), intent(in) :: nvals
	integer :: k
	print *, "	n	trapezoid	error	ratio"
	


	do k = nvals(1), nvals(size(nvals))
		call trapezoid(f,a,b,k,int_trap)
		
		error = abs(int_trap - int_true)
		ratio = last_error/error
		last_error = error
		print 11, k, int_trap, error, ratio
 11		format(i8, es22.14, es13.3, es13.3)
		enddo
end subroutine error_table








end module quadrature



	
