! $MYHPSC/homework3/intersections.f90

module functions

contains

real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt


real(kind=8) function gofx(x)
	implicit none
	real(kind=8), intent(in) :: x
    
	gofx = x * cos(x * 4.D0*DATAN(1.D0))+ 0.6 * x**2 -1.
end function gofx

real(kind=8) function gprime(x)
	implicit none
	real(kind=8), intent(in) :: x
    
	gprime = cos(x * 4.D0*DATAN(1.D0))-x*4.D0*DATAN(1.D0)*sin(x*4.D0*DATAN(1.D0))+1.2*x
end function gprime

end module functions
