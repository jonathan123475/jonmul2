
from newton import solve
import math
import numpy as np
import matplotlib.pyplot as plt

def fvals(x0):
	f= x0 * math.cos(x0 * math.pi)+ 0.6 * x0**2 -1.
	fp= math.cos(x0*math.pi)-x0*math.pi*math.sin(x0*math.pi)+1.2*x0
	return f, fp



x = np.linspace(-5,5,1001)
y= x * np.cos(x * np.pi)+ 0.6 * x**2 -1.
plt.figure(1)
plt.clf() 
plt.plot(x,y,'b-')

myarray= np.array([-2.1,1.4, -1.6, -0.7])
for i in range(len(myarray)):
	x= myarray[i]
	x01, iter1 =solve(fvals, x)
	y,yp = fvals(x01)
	plt.plot(x01, y,'ro')
	print " "
	print "With initial guess x0 = %22.15e," % (x)
	print "     solve returns x = %22.15e after %i iterations" % (x01, iter1)


plt.ylim(-2,8)         # set limits in y for plot

plt.title("Intersections for assignment 3")

plt.savefig('intersections.png')   

