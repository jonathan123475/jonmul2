

def solve(fvals, x0, debug=False):
	f, fp = fvals(x0)
	kmax=1000
	tol = 1.e-14
	t = 0
	if debug:
            	print "Initial guess: x = %22.15e" % (x0)
	for k in range(t,kmax):
		if abs(f)<tol:
			break
		x0 = x0 - f/fp
		f, fp = fvals(x0)
		if debug:
            		print "After %s iterations, x = %22.15e" % (k+1,x0)
	return x0,k
	
# $UWHPSC/codes/homework3/test_code.py 
# To include in newton.py

def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=True):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x

	
if __name__ == "__main__":

    print "Running test... "
    test1()
	