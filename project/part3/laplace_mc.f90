program laplace_mc
	use mc_walk
	use random_util, only: init_random_seed
	
	real(kind=8) :: x0 = 0.9
	real(kind=8) :: y0 = 0.6
	real(kind=8) :: u_mc, uerr
	!real(kind=8) :: dx = (bx-ax)/float(nx+1)
	!real(kind=8) :: dy = (by-ay)/float(ny+1)
	!integer :: nx = 19
	!integer :: ny = 11
	integer :: n_mc = 10
	!integer :: nwalks = 0
	integer :: seed1 = 12345
	integer :: i0 
	integer :: j0
	integer :: max_steps = 100*max(nx,ny)
	integer :: j, n_success
	call init_random_seed(seed1)
	!n_mc = 10
	n_success = 0
	i0 = NINT((x0-ax)/dx)
	j0 = NINT((y0-ay)/dy)
	x0 = ax + i0*dx
	y0 = ay + j0*dy
	open(unit=25, file='mc_laplace_error.txt', status='unknown')
	do j=1, 10
		call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
		n_mc = n_mc * 2
		uerr = u_mc - utrue(x0,y0)
		print *, n_success, u_mc, uerr
		write(25,'(i10,e23.15,e15.6)') n_success, u_mc, uerr
		enddo
	
	
	print *,"Final approximation to u(x0,y0):", u_mc
	print *,"Total number of random walks: ", nsuccess
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
end program laplace_mc
