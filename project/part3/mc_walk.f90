
module mc_walk

    implicit none
    real(kind=8), parameter :: ax = 0.0d0
    real(kind=8), parameter :: bx = 1.0d0
    real(kind=8), parameter :: ay = 0.4d0
    real(kind=8), parameter :: by = 1.0d0
    integer, parameter :: nx = 19
    integer, parameter :: ny = 11
    integer :: nwalks = 0
    real(kind=8), parameter :: dx = (bx - ax) / (nx+1)
    real(kind=8), parameter :: dy = (by - ay) / (ny+1)

contains

subroutine random_walk(i0, j0, max_steps, ub, iabort)
	
	implicit none
	real(kind=8), intent(inout) :: ub
	integer, intent(in) :: i0, j0, max_steps
	integer, intent(inout) :: iabort
	integer :: k, i, j
	real(kind=8), dimension(max_steps) :: rnum
	real(kind=8) :: xb, yb
	iabort = 0
	nwalks = nwalks+1
	do k=1, max_steps
		rnum(k) = rand()
		enddo
	
	do k=1, max_steps
		i = i0
		j = j0
		if (rnum(k) < 0.25)then
			i = i-1
		elseif (rnum(k) < .5)then
			i = i+1
		elseif (rnum(k) < .75)then
			j = j-1
		else
			j = j+1
			endif
		if (i*j*(nx+1-i)*(ny+1-j) == 0)then
			xb = ax + i*dx
			yb = ay + j*dy
			ub = uboundary(xb, yb)
			print *, "Hit boundary after this many steps:",j
			exit
			endif
		if (j== (max_steps - 1))then
			print *, "Did not hit boundary after this many steps :", j
			iabort = 1 
			endif
		enddo
end subroutine random_walk

subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
	
	
	implicit none
	real(kind=8), intent(out) :: u_mc
	integer, intent(in) :: i0, j0, max_steps, n_mc
	integer, intent(inout) :: n_success
	integer :: iabort, i, j
	integer :: k
	
	
	!local Vars
	
	real(kind=8) :: ub_sum, ub
	

	ub_sum = 0
	n_success = 0
	
	do k=1, n_mc
		i = i0
		j = j0
		call random_walk(i, j, max_steps,ub, iabort)

		if  (iabort == 0)then
			ub_sum = ub + ub_sum
			n_success = 1 + n_success
			endif
		enddo
	u_mc = ub_sum / n_success
	
	
	
	
	
end subroutine many_walks


function utrue(x, y)

    ! True solution for comparison, if known.

    implicit none
    real(kind=8), intent(in) :: x,y
    real(kind=8) :: utrue

    utrue = x**2 - y**2

end function utrue

function uboundary(x, y)

    ! Return u(x,y) assuming (x,y) is a boundary point.

    implicit none
    real(kind=8), intent(in) :: x,y
    real(kind=8) :: uboundary

    if ((x-ax)*(x-bx)*(y-ay)*(y-by) .ne. 0.d0) then
        print *, "*** Error -- called uboundary at non-boundary point"
        stop
        endif

    uboundary = utrue(x,y)   ! assuming we know this

end function uboundary
    

end module mc_walk
