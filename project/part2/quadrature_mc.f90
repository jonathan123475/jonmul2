
module quadrature_mc

contains

real(kind=8) function quad_mc(g, a, b, ndim, npoints)

    
     
    implicit none
    integer, intent(in) :: ndim, npoints
    real(kind=8), dimension(ndim),intent(in) :: a,b
    real(kind=8), dimension(ndim) :: x
    real(kind=8), external :: g
    

    ! Local variables:
    real(kind=8) :: randnum, gval, integralsum, area
    integer :: j, k
    gval= 0.d0
    integralsum = 0.d0
    do k=1, npoints
	area = 1.d0
    	do j=1,ndim
		randnum = rand()
		x(j) = (b(j) - a(j)) * randnum + a(j)
		area = area * (b(j) - a(j)) 
		enddo 
	gval= g(x,ndim)*area + gval
		
	enddo
   ! do j=1, ndim
	!integralsum =integralsum + (b(j) - a(j)) * gsum
	!enddo
    quad_mc = gval / npoints
end function quad_mc

end module quadrature_mc

