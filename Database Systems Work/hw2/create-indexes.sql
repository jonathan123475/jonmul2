-- Jonathan Muller

-- Speeds up join in #1, #2, and #5
create index indmidpid on casts(mid, pid);

--For clause in #1
create index indexmname on movie(name);

--Slight speed increase in where clause on #2
create index indexgenre on genre(genre);

--Speed up join in #2
create index indexmdirmid on movie_directors(mid);

--Slight speed increase for #2, #3, and #5
create index indmyear on movie(year); 

--Speed up the joins in #3 and #5.
create index castspid on casts(pid);

--Speed up the grouping of movie directors in #4.
create index  indmdirdid on movie_directors(did);

--Speed up the join on #4
create index mdidmid on movie_directors(did,mid);