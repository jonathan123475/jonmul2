--1 
-- Before
-- CPU Time: user 45.380691 sys 0.000000
-- After
-- CPU Time: user 0.000000 sys 0.000000

select act.fname, act.lname
from actor act, casts ct, movie m
where m.id = ct.mid 
and ct.pid = act.id 
and m.name = 'Officer 444';

--2 
-- Before
-- CPU Time: user 4.882831 sys 11.528474
-- After
-- CPU Time: user 0.000000 sys 0.000000 

select dir.fname, m.name, m.year
from movie m, movie_directors mdir, directors dir, genre gen
where gen.genre = 'Film-Noir'
and gen.mid = m.id
and m.year % 4 = 0 
and mdir.mid = m.id
and mdir.did = dir.id;

--3 
-- Before
-- CPU Time: user 58.250773 sys 1.653611
-- After
-- CPU Time: user 0.202801 sys 0.000000


select distinct act1.fname, act1.lname
from actor act1, casts ct1, movie m1, movie m2, casts ct2, actor act2
where m1.id = ct1.mid
and ct1.pid = act1.id
and m2.id = ct2.mid
and ct2.pid = act2.id
and act1.id = act2.id
and m1.year<1900
and m2.year > 2000;

--The movies that show are are reenactments where the year of the imdb table
-- is based off the year that the reenactment took place.

--4 
-- Before
-- CPU Time: user 1.513210 sys 0.124801
-- After
-- CPU Time: user 0.982806 sys 0.015600

select dir.fname, dir.lname, count(mdir.mid)
from movie_directors mdir, directors dir
where dir.id = mdir.did
group by mdir.did
having count(mdir.mid)>=500;

--5 
-- Before
-- CPU Time: user 45.723893 sys 0.062400
-- After
-- CPU Time: user 0.780005 sys 0.046800

select act.fname, act.lname, count(ct.role)
from actor act, casts ct, movie m
where m.year = 2010
and m.id = ct.mid
and ct.pid = act.id
group by ct.mid, ct.pid
having count(ct.role) >= 5 ;










