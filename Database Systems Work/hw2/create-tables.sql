-- Jonathan Mullerd
create table actor(id int primary key references casts,fname varchar(30),lname varchar(30),gender varchar(1));
create table movie(id int primary key,name varchar(150),year int);
create table directors(id int primary key,fname varchar(30),lname varchar(30));
create table casts(pid int references actor,mid int references movie,role varchar(50));
create table movie_directors(did int references directors,mid int references movie);
create table genre(mid int, genre varchar(50));

.import actor-ascii.txt actor
.import movie-ascii.txt movie
.import directors-ascii.txt directors
.import casts-ascii.txt casts
.import movie_directors-ascii.txt movie_directors
.import genre-ascii.txt genre


