import java.util.Properties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.io.FileInputStream;

/**
 * Runs queries against a back-end database
 */
public class Query {
	private String configFilename;
	private Properties configProps = new Properties();

	private String jSQLDriver;
	private String jSQLUrl;
	private String jSQLCustomer;
	private String jSQLUser;
	private String jSQLPassword;

	// DB Connection
	private Connection conn;
    private Connection customerConn;

	// Canned queries

	// LIKE does a case-insensitive match
	private static final String SEARCH_SQL_BEGIN =
		"SELECT * FROM movie WHERE name LIKE '%";
	private static final String SEARCH_SQL_END = 
		"%' ORDER BY id";

	private static final String DIRECTOR_MID_SQL = "SELECT y.* "
					 + "FROM movie_directors x, directors y "
					 + "WHERE x.mid = ? and x.did = y.id";
	private PreparedStatement directorMidStatement;

	/* uncomment, and edit, after your create your own customer database */
	
	private static final String CUSTOMER_LOGIN_SQL = 
		"SELECT * FROM customer WHERE login = ? and password = ?";
	private PreparedStatement customerLoginStatement;

	private static final String BEGIN_TRANSACTION_SQL = 
		"SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; BEGIN TRANSACTION;";
	private PreparedStatement beginTransactionStatement;

	private static final String COMMIT_SQL = "COMMIT TRANSACTION";
	private PreparedStatement commitTransactionStatement;

	private static final String ROLLBACK_SQL = "ROLLBACK TRANSACTION";
	private PreparedStatement rollbackTransactionStatement;
	
	

	public Query(String configFilename) {
		this.configFilename = configFilename;
	}

    /**********************************************************/
    /* Connection code to SQL Azure. Example code below will connect to the imdb database on Azure
       IMPORTANT NOTE:  You will need to create (and connect to) your new customer database before 
       uncommenting and running the query statements in this file .
     */

	public void openConnection() throws Exception {
		configProps.load(new FileInputStream(configFilename));

		jSQLDriver   = configProps.getProperty("videostore.jdbc_driver");
		jSQLUrl	   = configProps.getProperty("videostore.imdb_url");
		jSQLCustomer =  configProps.getProperty("videostore.customer_url");
		jSQLUser	   = configProps.getProperty("videostore.sqlazure_username");
		jSQLPassword = configProps.getProperty("videostore.sqlazure_password");
		
		//myUrl = configProps.getProperty("videostore.customer_url");


		/* load jdbc drivers */
		Class.forName(jSQLDriver).newInstance();

		/* open connections to the imdb database */

		conn = DriverManager.getConnection(jSQLUrl, // database
						   jSQLUser, // user
						   jSQLPassword); // password
                
		conn.setAutoCommit(true); //by default automatically commit after each statement 

		/* You will also want to appropriately set the 
                   transaction's isolation level through:  */
		conn.setTransactionIsolation(8);

		/* Also you will put code here to specify the connection to your
		   customer DB.  E.g.*/

		customerConn = DriverManager.getConnection(jSQLCustomer,jSQLUser,jSQLPassword);
		customerConn.setAutoCommit(true); //by default automatically commit after each statement
		customerConn.setTransactionIsolation(8); //
		
	        
	}

	public void closeConnection() throws Exception {
		conn.close();
		customerConn.close();
	}

    /**********************************************************/
    /* prepare all the SQL statements in this method.
      "preparing" a statement is almost like compiling it.  Note
       that the parameters (with ?) are still not filled in */

	public void prepareStatements() throws Exception {

		directorMidStatement = conn.prepareStatement(DIRECTOR_MID_SQL);

		/* uncomment after you create your customers database */
		/**/
		customerLoginStatement = customerConn.prepareStatement(CUSTOMER_LOGIN_SQL);
		beginTransactionStatement = customerConn.prepareStatement(BEGIN_TRANSACTION_SQL);
		commitTransactionStatement = customerConn.prepareStatement(COMMIT_SQL);
		rollbackTransactionStatement = customerConn.prepareStatement(ROLLBACK_SQL);
		

		/* add here more prepare statements for all the other queries you need */
		/* . . . . . . */
	}


    /**********************************************************/
    /* Suggested helper functions; you can complete these, or write your own
       (but remember to delete the ones you are not using!) */

	public int getRemainingRentals(int cid) throws Exception {
		/* How many movies can she/he still rent?
		   You have to compute and return the difference between the customer's plan
		   and the count of outstanding rentals */
		return (99);
	}

	public String getCustomerName(int cid) throws Exception {
		/* Find the first and last name of the current customer. */
		return ("JoeFirstName" + " " + "JoeLastName");

	}

	public boolean isValidPlan(int planid) throws Exception {
		/* Is planid a valid plan ID?  You have to figure it out */
		return true;
	}

	public boolean isValidMovie(int mid) throws Exception {
		/* is mid a valid movie ID?  You have to figure it out */
		return true;
	}

	private int getRenterID(int mid) throws Exception {
		/* Find the customer id (cid) of whoever currently rents the movie mid; return -1 if none */
		return (77);
	}

    /**********************************************************/
    /* login transaction: invoked only once, when the app is started  */
	public int transaction_login(String name, String password) throws Exception {
		/* authenticates the user, and returns the user id, or -1 if authentication fails */

		/* Uncomment after you create your own customers database */
		
		int cid;

		customerLoginStatement.clearParameters();
		customerLoginStatement.setString(1,name);
		customerLoginStatement.setString(2,password);
		ResultSet cid_set = customerLoginStatement.executeQuery();
		if (cid_set.next()) cid = cid_set.getInt(1);
		else cid = -1;
		cid_set.close();
		return(cid);
		
		
	}

	public void transaction_printPersonalData(int cid) throws Exception {
		/* println the customer's personal data: name, and plan number */
		System.out.println("Welcome Customer number "+cid+"!");
	}


    /**********************************************************/
    /* main functions in this project: */

	public void transaction_search(int cid, String movie_title)
			throws Exception {
		/* searches for movies with matching titles: SELECT * FROM movie WHERE name LIKE movie_title */
		/* prints the movies, directors, actors, and the availability status:
		   AVAILABLE, or UNAVAILABLE, or YOU CURRENTLY RENT IT */

		/* Interpolate the movie title into the SQL string */
		if(movie_title.contains(";")){
			return;
		}
		String searchSql = SEARCH_SQL_BEGIN + movie_title + SEARCH_SQL_END;
		
		Statement searchStatement = conn.createStatement();
		ResultSet movie_set = searchStatement.executeQuery(searchSql);
		while (movie_set.next()) {
			int mid = movie_set.getInt(1);
			System.out.println("ID1: " + mid + " NAME: "
					+ movie_set.getString(2) + " YEAR: "
					+ movie_set.getString(3));
			/* do a dependent join with directors */
			directorMidStatement.clearParameters();
			directorMidStatement.setInt(1, mid);
			ResultSet director_set = directorMidStatement.executeQuery();
			while (director_set.next()) {
				System.out.println("\t\tDirector: " + director_set.getString(3)
						+ " " + director_set.getString(2));
			}
			director_set.close();
			//list Actors
			String actorsSql = "select  a.lname, a.fname from actor a, movie m, casts c where m.id = "+mid+" and c.pid = a.id and c.mid = m.id;";
			Statement actorStatement = conn.createStatement();
			ResultSet actorset = actorStatement.executeQuery(actorsSql);
			System.out.println("\t\tActors:");
			while(actorset.next()){
				System.out.println("\t\t\tName: "+actorset.getString(1) + ", "+actorset.getString(2));
			}
			//Check status
			String searchSql1 = "select * from rental where mid = "+mid+";";
			Statement rentStatement = customerConn.createStatement();
			ResultSet rentalq = rentStatement.executeQuery(searchSql1);
			if(rentalq.next()){
				if(rentalq.getString(3).equals("open")){
					if(rentalq.getInt(1) == cid){
						System.out.println("\t\tStatus: YOU HAVE IT");
					}else{
						System.out.println("\t\tStatus: UNAVAILABLE");
					}
				}else{
					System.out.println("\t\tStatus: AVAILABLE");
				}
				
				while (rentalq.next()) {
					if(rentalq.getString(3).equals("open")){
						
						
						if(rentalq.getInt(1) == cid){
							System.out.println("\t\tStatus: YOU HAVE IT");
						}else{
							System.out.println("\t\tStatus: UNAVAILABLE");
						}
						
					}else{
						System.out.println("\t\tStatus: AVAILABLE");
					}
				}		
			}else{
				System.out.println("\t\tStatus: AVAILABLE");
			}
			/* now you need to retrieve the actors, in the same manner */
			/* then you have to find the status: of "AVAILABLE" "YOU HAVE IT", "UNAVAILABLE" */
		}
		
		movie_set.close();
		System.out.println();
		
	}

	public void transaction_choosePlan(int cid, int pid) throws Exception {
	    /* updates the customer's plan to pid: UPDATE customer SET plid = pid */
	    /* remember to enforce consistency ! */
		if(pid > 0 && pid <5 ){
			try {
				beginTransaction();
				String updatesql = "UPDATE customer set pid = " + pid + " where id = "+ cid+";";
				Statement updateStatement = customerConn.createStatement();
				updateStatement.executeUpdate(updatesql);
				commitTransaction();
			} catch (Exception e) {
				rollbackTransaction();
			}
		} else{
			System.out.println();
			System.out.println("You must pick a Valid Plan ID. \n Valid plan ID's are 1-4");
		}
	}

	public void transaction_listPlans() throws Exception {
	    /* println all available plans: SELECT * FROM plan */
		try {
			beginTransaction();
			String searchSql = "Select * from service;";
			Statement planStatement = customerConn.createStatement();
			ResultSet services = planStatement.executeQuery(searchSql);
			commitTransaction();
		
			while (services.next()) {
				System.out.println("Plan: " + services.getInt(1)+ " NAME: "
						+ services.getString(2) + " Max Rentals: "
						+ services.getInt(3)+ " Monthly Fee: "
						+ services.getInt(4));
			}
		} catch (Exception e) {
			rollbackTransaction();
		}
	}

	public void transaction_rent(int cid, int mid) throws Exception {
	    /* rent the movie mid to the customer cid */
	    /* remember to enforce consistency ! */
		try {
			beginTransaction();
			String searchSql = "select * from rental where mid = "+mid+";";
			Statement rentStatement = customerConn.createStatement();
			ResultSet rentalq = rentStatement.executeQuery(searchSql);
			commitTransaction();
			while (rentalq.next()) {
				if(rentalq.getString(3).equals("closed")){
					//if the movie is available just move on to next line
				} else {
					System.out.println("This movie is unavailable.");
					if(rentalq.getInt(1) == cid){
						System.out.println("You currently have this movie rented.");
					}
					return;
				}
			}
		} catch (Exception e) {
			rollbackTransaction();
		}
		try {
			beginTransaction();
			String countSql = "select count(*) from rental where id = "+cid+" and status = 'open';";
			Statement countStatement = customerConn.createStatement();
			ResultSet countq = countStatement.executeQuery(countSql);
			commitTransaction();
			countq.next();
			int currentrentals = countq.getInt(1);
			beginTransaction();
			String serviceSql = "select * from customer where id = "+cid+";";
			Statement serviceStatement = customerConn.createStatement();
			ResultSet serviceq = countStatement.executeQuery(serviceSql);
			commitTransaction();
			serviceq.next();
			int allowedrentals = serviceq.getInt(2);
			if(currentrentals >= allowedrentals){
				System.out.println("You are at your max rentals.");
				System.out.println("You must upgarde your plan or first return a movie.");
				return;
			}
		} catch (Exception e) {
			rollbackTransaction();
		}
		try {
			beginTransaction();
			//is this an actual movie?
			String ismovieSql = "select count(*) from movie where id = "+mid+";";
			Statement ismovieStatement = conn.createStatement();
			ResultSet ismovieq = ismovieStatement.executeQuery(ismovieSql);
			commitTransaction();
			ismovieq.next();
			if(ismovieq.getInt(1) ==0){
				System.out.println("This is not a valid movie.");
				System.out.println("Please enter a valid movie ID.");
				return;
			}
			
			//rent movie
			beginTransaction();
			String updatesql = "insert into rental values ("+cid+","+mid+",'open',"+System.currentTimeMillis()+");";
			Statement updateStatement = customerConn.createStatement();
			updateStatement.executeUpdate(updatesql);
			commitTransaction();
		} catch (Exception e) {
			rollbackTransaction();
		}
	}

	public void transaction_return(int cid, int mid) throws Exception {
	    /* return the movie mid by the customer cid */
		
		//See if the movie is actually rented
		String countSql = "select count(*) from rental where id = "+cid+" and mid = "+mid+" and status = 'open';";
		Statement countStatement = customerConn.createStatement();
		ResultSet countq = countStatement.executeQuery(countSql);
		countq.next();
		if(countq.getInt(1) == 0){
			System.out.println("You don't currently have this movie rented.");
		}else{
			String updatesql = "update rental set status = 'closed' where id = "+cid+" and mid = "+mid+";";
			Statement updateStatement = customerConn.createStatement();
			updateStatement.executeUpdate(updatesql);
		}
	}

	public void transaction_fastSearch(int cid, String movie_title)
			throws Exception {
		if(movie_title.contains(";")){
			return;
		}
		//List of all movies
		String movieSql = "select * from movie where name like '%"+movie_title+"%' order by id;";
		Statement movieStatement = conn.createStatement();
		ResultSet movieq = movieStatement.executeQuery(movieSql);
		
		//List of all directors
		String directorSql = "SELECT m.id, d.fname, d.lname FROM MOVIE_DIRECTORS md, DIRECTORS d, MOVIE m" + " WHERE md.mid = m.id and md.did = d.id"+ " and m.name LIKE '%" + movie_title + "%'" + " group by m.id, d.fname, d.lname"+ " order by m.id";
		Statement directorStatement = conn.createStatement();
		ResultSet directorq = directorStatement.executeQuery(directorSql);
		
		//List of all actors of all movies
		String actorSql = "SELECT m.id, a.fname, a.lname FROM actor a, casts c, movie m"+ " where a.id = c.pid and m.id = c.mid"+ " and m.name LIKE '%" + movie_title + "%'" + " group by m.id, a.fname, a.lname"+ " order by m.id";
		Statement actorStatement = conn.createStatement();
		ResultSet actorq = actorStatement.executeQuery(actorSql);
		
		directorq.next();
		actorq.next();
		while(movieq.next()){
			int mid = movieq.getInt(1);
			String name = movieq.getString(2);
			int year = movieq.getInt(3);
			System.out.println("ID: " + mid + "  NAME: " + name + "  YEAR: " + year);
			
			//all directors of this movie
			while(!directorq.isAfterLast() && directorq.getInt(1)==mid){
				System.out.println("\t\tDirector: " + directorq.getString(2)+ " " + directorq.getString(3));
				directorq.next();
			}
			//all acotrs of this movie
			System.out.println("\t\tActors: ");
			while(!actorq.isAfterLast() && actorq.getInt(1)==mid){
				System.out.println("\t\t\tName: "+actorq.getString(2) + " " + actorq.getString(3));
				actorq.next();
			}
			
		}
		
		
	}


    /* Uncomment helpers below once you've got beginTransactionStatement,
       commitTransactionStatement, and rollbackTransactionStatement setup from
       prepareStatements():*/
    
       public void beginTransaction() throws Exception {
	    customerConn.setAutoCommit(false);
	    beginTransactionStatement.executeUpdate();	
        }

        public void commitTransaction() throws Exception {
	    commitTransactionStatement.executeUpdate();	
	    customerConn.setAutoCommit(true);
	}
        public void rollbackTransaction() throws Exception {
	    rollbackTransactionStatement.executeUpdate();
	    customerConn.setAutoCommit(true);
	    } 
    

}
