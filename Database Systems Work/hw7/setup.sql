create table Service(pid int primary key, name varchar(max), maxnumber int, monthlyfee int);
create table Customer(id int primary key,pid int references Service(pid) , login varchar(max), password varchar(max), fname varchar(max), lname varchar(max));
create table Rental(id int references Customer(id), mid int, status varchar(max), date varchar(max));
CREATE CLUSTERED INDEX RentalIndex ON Rental (id);


insert into service values(1,'basic',2,5);
insert into service values(2,'standard',3,10);
insert into service values(3,'extra',4,15);
insert into service values(4,'super',5,20);

insert into Customer values (01,1,'jon','jon1','jon','muller');
insert into Customer values (02,2,'ylan','ylan1','ylan','to');
insert into Customer values (03,3,'cam','cam1','cam','dry');
insert into Customer values (04,4,'joe','joe1','joe','frank');