register s3n://uw-cse344-code/myudfs.jar

raw = LOAD 's3n://uw-cse344/btc-2010-chunk-000' USING TextLoader as (line:chararray);



ntriples = foreach raw generate FLATTEN(myudfs.RDFSplit3(line)) as (subject:chararray,predicate:chararray,object:chararray);
ntriples2 = foreach raw generate FLATTEN(myudfs.RDFSplit3(line)) as (subject2:chararray,predicate2:chararray,object2:chararray);

filter1 = FILTER ntriples BY (subject matches '.*rdfabout\\.com.*') PARALLEL 50;

filter2 = FILTER ntriples2 BY (subject2 matches '.*rdfabout\\.com.*') PARALLEL 50;

objectjoin=  JOIN filter1 BY object, filter2 BY subject2 PARALLEL 50;
objectdistinct = DISTINCT objectjoin PARALLEL 50;
--resultdata = foreach objects2 generate $0, $1, $2, $3, $4, $5   PARALLEL 50;

store objectdistinct into 's3n://jonmul/p3p2' using PigStorage();
