register s3n://uw-cse344-code/myudfs.jar

raw = LOAD 's3n://uw-cse344' USING TextLoader as (line:chararray);

ntriples = foreach raw generate FLATTEN(myudfs.RDFSplit3(line)) as (subject:chararray,predicate:chararray,object:chararray);

objects = group ntriples by (subject) PARALLEL 50;


count_by_object = foreach objects generate flatten($0), COUNT($1) PARALLEL 50;


groupcount = group count_by_object by ($1) PARALLEL 50;

groupeach = foreach groupcount generate ($0), COUNT($1) PARALLEL 50;

count_by_object_ordered = order groupeach by ($0) PARALLEL 50;

store count_by_object_ordered into 's3n://jonmul/p4' using PigStorage();
