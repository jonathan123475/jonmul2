<!--
Jonathan Muller
10/25/2012
cse154-Autumn
Homework 4
Enjoy =D
-->
<?php
include("common.php");
$name=$_GET["name"];
$namelist=file("singles.txt");
$namevalues;
foreach ($namelist as $index){
	$info=explode(",",$index);
	if($info[0]==$name){
		$namevalues=$info;
		break;
	}
}
$gender=$namevalues[1];
$age=$namevalues[2];
$personality=$namevalues[3];
$os=$namevalues[4];
$min=$namevalues[5];
$max=$namevalues[6];
?>
<html>
	<?php
		headitems();
	?>
	<body>
		<?php
			nerdluvheader();
		?>
		<h1>Matches for <?=$name?></h1>
		<?php
			foreach ($namelist as $single){
				$info=explode(",",$single);
				$type=$info[3];
				$p1=str_split($personality);
					$p2=str_split($type);
				if($gender!=$info[1] &&
					$age>=$info[5] &&
					$age<=$info[6] &&
					$os==$info[4] &&
					$min<=$info[2] &&
					$max>=$info[2] &&
					typematch($p1,$p2)){
					#typematch is a script in common, it matches the personality
					#	type based off of the specifications.
					#Run the script from common.php to print the info here
					printnameblock($info);
				}
			}
		?>
		<?php
			returntext();
			validatepage();
		?>
	</body>
	
</html>