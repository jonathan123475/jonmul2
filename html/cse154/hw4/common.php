<!--
Jonathan Muller
10/25/2012
cse154-Autumn
Homework 4
Enjoy =D

This contains common code throughout the webpages
-->
<!--
	headitems() is the header that belongs on every webpage
-->
<?php
function headitems(){ ?>
<head>
	<title>NerdLuv</title>
		
	<meta charset="utf-8" />
		
	<!-- instructor-provided CSS and JavaScript links; do not modify -->
	<link href="https://webster.cs.washington.edu/images/nerdluv/heart.gif" type="image/gif" rel="shortcut icon" />
	<link href="https://webster.cs.washington.edu/css/nerdluv.css" type="text/css" rel="stylesheet" />
		
	<script src="http://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js" type="text/javascript"></script>
	<script src="https://webster.cs.washington.edu/js/nerdluv/provided.js" type="text/javascript"></script>
</head>



<?php } ?>

<!--
	nerdluvheader() is the banner area that is at the top of each webpage
-->
<?php
function nerdluvheader() { ?>
<div id="bannerarea">
	<img src="https://webster.cs.washington.edu/images/nerdluv.png" alt="nerdLuv" /><br />
	where meek geeks meet
</div>


<?php } ?>

<!--
	returntext() is the paragraph and back-button at 
	the bottom of each webpage
-->
<?php
function returntext() { ?>
<div>
	<p>
		This page is for single nerds to meet and date each other!  Type in your personal information and wait for the nerdly luv to begin!  Thank you for using our site.
	</p>
			
	<p>
		Results and page (C) Copyright NerdLuv Inc.
	</p>
	<ul>
		<li>
			<a href="index.php">
				<img src="https://webster.cs.washington.edu/images/nerdluv/back.gif" alt="icon" />
				Back to front page
			</a>
		</li>
	</ul>
</div>

<?php } ?>
<!--
	validatepage() is the page validators for each page.
	This is seperate from returntext() out of personal preference,
	though they could be put together. I think it is easier to know
	what is there when reading the code when the two are seperate.
-->
<?php
function validatepage() { ?>
<div id="w3c">
	<a href="https://webster.cs.washington.edu/validate-html.php"><img src="https://webster.cs.washington.edu/images/w3c-html.png" alt="Valid HTML" /></a>
	<a href="https://webster.cs.washington.edu/validate-css.php"><img src="https://webster.cs.washington.edu/images/w3c-css.png" alt="Valid CSS" /></a>
</div>
<?php } ?>

<!--
	printnameblock() takes in the array of the match found and prints out
	the required box of info.
-->
<?php
function printnameblock(array $info) { ?>
<div class="match">
	<p>
		<img src="https://webster.cs.washington.edu/images/nerdluv/user.jpg" />
		<?=$info[0] ?>
	</p>
	<ul>
		<li><strong>gender:</strong><?=$info[1]?></li>
		<li><strong>age:</strong><?=$info[2]?></li>
		<li><strong>type:</strong><?=$info[3]?></li>
		<li><strong>OS:</strong><?=$info[4]?></li>
	</ul>
</div>
<?php } ?>

<!--
	typematch() compares the two user's personality type
	and returns true if one of their letter types match.
	This particular code works even if the user entered their type
	out of order.
-->
<?php
function typematch(array $p1,array $p2){
	foreach ($p1 as $char1){
		foreach ($p2 as $char2){
			if($char1==$char2){
				return true;
			}
		}
	}
}
?>