<!--
Jonathan Muller
10/25/2012
cse154-Autumn
Homework 4
Enjoy =D
-->
<?php
include("common.php");


?>
<!DOCTYPE html>
<html>
	<?php
		headitems();
	?>
	<body>
		<?php
			nerdluvheader();
		?>
		<div>
			<form action="signup-submit.php" method="post">
				<fieldset>
					<legend>New User Signup:</legend>
					<ul>
						<li>
							<strong>Name:</strong>
							<input type="text" maxlength="16" name="name" />
						</li>
						<li>
							<strong>Gender:</strong>
							<label><input type="radio" name="gender" value="M" />Male</label>
							<label><input type="radio" name="gender" value="F" />Female</label>
						</li>
						<li>
							<strong>Age:</strong>
							<input type="text" size="6" maxlength="2" name="age" />
						</li>
						<li>
							<strong>Personality type:</strong>
							<input type="text" size="6" maxlength="4" name="personality" />
							(<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't know your type?</a>)
						</li>
						<li>
							<strong>Favorite OS:</strong>
							<select name="favoriteos" >
								<option selected="selected">Windows</option>
								<option>Mac OS X</option>
								<option>Linux</option>
							</select>
						</li>
						<li>
							<strong>Seeking age:</strong>
							<input type="text" size="6" maxlength="2" name="minage" value="min" onFocus="this.value=''" />to
							<input type="text" size="6" maxlength="2" name="maxage" value="max" onFocus="this.value=''" />
						</li>
					</ul>	
					<input type="submit" value="Sign Up" />
				</fieldset>
			</form>
		</div>
		<?php
			returntext();
			validatepage();
		?>
		
	</body>

</html>