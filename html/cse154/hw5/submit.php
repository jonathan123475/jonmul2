<!--
Jonathan Muller
11/1/2012
cse154-Autumn
Homework 5
Enjoy =D
-->

<?php
session_start();
$name=$_SESSION["name"];
$action=$_POST["action"];
#check which action was passed
#this page does not alter anything if no action is passed
# i.e. malicious user visits this page.
if($action == "add"){
	$item=$_POST["item"];
	file_put_contents("todo_".$name.".txt",$item."\n",FILE_APPEND);
}else if($action=="delete"){
	$index=$_POST["index"];
	$file=file("todo_".$name.".txt");
	unset($file[$index]);
	$file= array_values($file);
	file_put_contents("todo_".$name.".txt",$file);
}


header("Location: todolist.php"); 
die();
?>