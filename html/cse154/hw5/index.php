<!--
Jonathan Muller
11/1/2012
cse154-Autumn
Homework 5
Enjoy =D
-->
<!DOCTYPE html>
<?php

if(!isset($_COOKIE["logindate"])){
	$expireTime = time() + 60*60*24*7;
	$time=time();
	$currentdate=date("D y M d, g:i:s a");
	setcookie("logindate",$currentdate, $expireTime);
	$lastdate=$currentdate;
}else{
	$lastdate=$_COOKIE["logindate"];
}
include("common.php");
?>


<html>
	<?php makeheader() ?>

	<body>
		<?php makeheadfoot() ?>

		<div id="main">
			<p>
				The best way to manage your tasks. <br />
				Never forget the cow (or anything else) again!
			</p>

			<p>
				Log in now to manage your to-do list. <br />
				If you do not have an account, one will be created for you.
			</p>

			<form id="loginform" action="login.php" method="post">
				<div><input name="name" type="text" size="8" autofocus="autofocus" /> <strong>User Name</strong></div>
				<div><input name="password" type="password" size="8" /> <strong>Password</strong></div>
				<div><input type="submit" value="Log in" /></div>
			</form>

			<p>
				<em>(last login from this computer was <?=$lastdate ?>)</em>
			</p>
		</div>

		<?php makefooter() ?>
	</body>
</html>
