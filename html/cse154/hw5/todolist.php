<!--
Jonathan Muller
11/1/2012
cse154-Autumn
Homework 5
Enjoy =D
-->
<!DOCTYPE html>
<?php
session_start();
$name=$_SESSION["name"];
$newuser=false;
if(!file_exists("todo_".$name.".txt")){
	file_put_contents("todo_".$name.".txt","");
	$newuser=true;
}
$file=file("todo_".$name.".txt");
$tasknumber=0;
$date=$_COOKIE["logindate"];

include("common.php");
?>

<html>
	<?php makeheader() ?>

	<body>
		<?php makeheadfoot() ?>

		<div id="main">
			<h2><?=$name ?>'s To-Do List</h2>

			<ul id="todolist">
				<?php
				if(!$newuser){
					foreach($file as $task){
					?>
					<li>
						<form action="submit.php" method="post">
							<input type="hidden" name="action" value="delete" />
							<input type="hidden" name="index" value=<?=$tasknumber ?> />
							<input type="submit" value="Delete" />
						</form>
						<?=$task ?>
					</li>
				<?php 
						$tasknumber++;
					} 	
				}?>
				
				<li>
					<form action="submit.php" method="post">
						<input type="hidden" name="action" value="add" />
						<input name="item" type="text" size="25" autofocus="autofocus" />
						<input type="submit" value="Add" />
					</form>
				</li>
			</ul>

			<div>
				<a href="logout.php"><strong>Log Out</strong></a>
				<em>(logged in since <?=$date ?>)</em>
			</div>

		</div>

		<?php makefooter() ?>
	</body>
</html>
