
//Jonathan Muller
//11/24/2012
//cse154-Autumn
//Homework 8b
//Enjoy =D

"use strict";

//Declaring global vars to keep track of the picture and the white square position
var currentpic = "background1.jpg";
var whiteleft=300;
var whitetop=300;
var wins=0;

window.onload = function() {
	//Checks if the wins value is stored already, and initiates it.
	//alert(localStorage.getItem("wins"));
	if(!(localStorage.getItem("wins")=== null)){
		wins=localStorage.getItem("wins");
	}
	if(!(localStorage.getItem("currentpic")=== null)){
		currentpic=localStorage.getItem("currentpic");
	}
	var bg = document.getElementById("background");
	//Setting the drop-down menu value.
	bg.selectedIndex=currentpic.charAt(10)-1;
	//Sets the Wins value
	document.getElementById("wins").innerHTML="Total wins: "+ wins;
	//Change the picture when the user requests
	document.getElementById("background").onchange = changepicture;
	document.getElementById("shuffle").onclick = shuffle;
	var area = document.getElementById("puzzlearea");
	for(var i=0;i<15;i++){
		var div = document.createElement("div");
		div.className="tile";
		div.innerHTML = i;
		//Set the position of each tile
		div.style.left = Math.floor((i%4))*100+ "px";
		div.style.top = Math.floor((i/4))*100 + "px";
		//Set the background of tile
		div.style.backgroundImage = 'url("'+currentpic+'")';
		div.style.cursor="default";
		//Mousover/click calls, see function for details
		div.onmouseover = picmouseover;
		div.onmouseout = picmouseout;
		div.onclick = picmove;
		//The position of the image on the tile
		var position = (-Math.floor((i%4))*100) + "px "+ (-Math.floor((i/4))*100)+ "px";
		div.style.backgroundPosition = position;
		area.appendChild(div);
	}

};

function shuffle() {
	document.getElementById("youwon").innerHTML="";
	var neighbors = document.querySelectorAll(".tile");
	var moveables = [];
	
	for(var i=0;i<1000;i++){
		//Checks all tiles to find neighbors of White Tile
		for(var j=0;j<neighbors.length; j++){
			//If tile is a neighbor, add it to the moveable array
			if(movable(neighbors[j])){
				moveables[moveables.length] = neighbors[j];
			}
		}
		//Move a random Neighbor of the White Tile.
		move(moveables[Math.floor((Math.random()*moveables.length))]);
		moveables = [];
	}
}

//Changes the picture when the user changes the option
function changepicture(){
	currentpic= this.value;
	var allParas = document.querySelectorAll(".tile");
	for (var i = 0; i < allParas.length; i++) {
		
		allParas[i].style.backgroundImage = 'url("'+currentpic+'")';
	}
	localStorage.setItem("currentpic",currentpic);
	
}


//On mouseover, checks if the tile is next to the white tile, and does the appropriate actions
function picmouseover() {
	if(movable(this)){
		this.style.color = "red";
		this.style.cursor="pointer";
		this.style.borderColor="red";
	}
}
//Reverts the changes when the user is no longer hovering over a tile
function picmouseout() {
	this.style.color = "black";
	this.style.cursor="default";
	this.style.borderColor="black";
}
//Checks if the object is next to the white tile
function movable(obj) { 
	var leftval = parseInt(obj.style.left);
	var topval = parseInt(obj.style.top);
	var leftdif = Math.abs(leftval - whiteleft);
	var topdif = Math.abs(topval - whitetop);
	return whiteleft==leftval && topdif==100 ||whitetop==topval && leftdif==100 ;

}

//Moves the picture/tile when the user clicks on a valid piece
function picmove() {
	document.getElementById("youwon").innerHTML="";
	if(movable(this)){
		move(this);
	}
	//If the puzzle is solved, do the required win text and update wins.
	if(solved()){
		document.getElementById("youwon").innerHTML="Congratulations! You won!";
		wins++;
		document.getElementById("wins").innerHTML="Total wins: "+ wins;
		localStorage["wins"]=wins;
	}
}


//Checks if the puzzle is solved.
function solved(){
	var tiles = document.querySelectorAll(".tile");
	
	//querySelectorAll returns an array of the tile objects.
	//Interesting enough, the array is in the winning order required, so
	//I simply run the same forumala that was used to set the positions of each tile
	//and if each position matches the position of the tile, then it is "solved"
	for(var i=0;i<tiles.length;i++){
		var left = Math.floor((i%4))*100+ "px";
		var top = Math.floor((i/4))*100 + "px";
		if(left!=tiles[i].style.left || top!=tiles[i].style.top){
			return false;
		}
	}
	return true;
}

//Move helper function. Moves the object tile to the position of the White Tile.
//The methods that use this have established that the tile is movable.
function move(object){
	var leftval = parseInt(object.style.left);
	var topval = parseInt(object.style.top);
	object.style.left = whiteleft+ "px";
	object.style.top = whitetop + "px";
	whiteleft=leftval;
	whitetop=topval;
}




