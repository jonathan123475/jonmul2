<!--
Jonathan Muller
10/17/2012
cse154-Autumn
Homework 3
Enjoy =D
-->

<?php
	$movie = $_GET["film"];
	$info = file("$movie/info.txt");
	$overview = file("$movie/overview.txt");
	$overviewpng = "$movie/overview.png";
	$reviews = glob("$movie/review*.txt");
	# Splitting the reviews in half, for each side.
	$reviewsize = sizeof($reviews);
	$reviewsleft = array_slice($reviews,0,($reviewsize+1)/2);
	$reviewsright = array_slice($reviews, ($reviewsize+1)/2);
	if($info[2]>=60){
		$ratingpic = "freshlarge.png"; 
	} else{
		$ratingpic = "rottenlarge.png";
	}
	# OVERVIEW VARS
	
	# TEST CODE
	# print_r(file("$reviewsleft[0]"));
	# print_r($reviewsright);
	# print_r($overview);
	# print_r(sizeof($reviews)); for halving the reviews
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Rancid Tomatoes</title>

		<meta charset="utf-8" />
		<link href="movie.css" type="text/css" rel="stylesheet" />
	</head>

	<body>
		<div class="rancbg">
			<img class="topbannerimg" src="https://webster.cs.washington.edu/images/rancidbanner.png" alt="Rancid Tomatoes" />
		</div>

		<h1><?=$info[0] ?> (<?=trim($info[1])?>)</h1>
		<div class="overallcontent">
			<div class="rating">
				<img src="https://webster.cs.washington.edu/images/<?=$ratingpic ?>" alt="Rotten" />
				<span class="overallrating"><?=$info[2] ?>%</span>
			</div>
			<div class="rightside">
				<div>
					<img src="<?=$overviewpng ?>" alt="general overview" />
				</div>
				
				<dl>
				<?php
						foreach ($overview as $block){
							$blockarray = explode(":",$block);
							?>
							<dt><?=$blockarray[0]?></dt>
							<dd><?=$blockarray[1] ?></dd>
					<?php } ?>
				</dl>
			</div>
			<div class="leftside">	
				
				<div class="leftcol">
					<?php
						foreach ($reviewsleft as $reviewfile){
						
						$currentfile=file("$reviewfile"); 
						?>
						
						<div>	
							<p class="p1">
								<img class="icons" src="https://webster.cs.washington.edu/images/<?=strtolower($currentfile[1]) ?>.gif" alt="Rotten" />
								<q><?=$currentfile[0]  ?></q>
							</p>
							<p class="p2">
								<img class="icons" src="https://webster.cs.washington.edu/images/critic.gif" alt="Critic" />
								<?=$currentfile[2]  ?> <br />
								<span><?=$currentfile[3] ?></span>
							</p>
						</div>
						
					<?php } ?>
					
				</div>
				<div class="rightcol">
					<?php
						foreach ($reviewsright as $reviewfile){
						
						$currentfile=file("$reviewfile"); 
						?>
						
						
						<p class="p1">
							<img class="icons" src="https://webster.cs.washington.edu/images/<?=strtolower($currentfile[1]) ?>.gif" alt="Rotten" />
							<q><?=$currentfile[0]  ?></q>
						</p>
						<p class="p2">
							<img class="icons" src="https://webster.cs.washington.edu/images/critic.gif" alt="Critic" />
							<?=$currentfile[2]  ?> <br />
							<span><?=$currentfile[3] ?></span>
						</p>
						
						
					<?php } ?>
				
				</div>
			</div>
			<p class="bottombar">(1-<?=$reviewsize?>) of <?=$reviewsize?></p>
			<div class="rating">
				<img src="https://webster.cs.washington.edu/images/<?=$ratingpic ?>" alt="Rotten" />
				<span class="overallrating"><?=$info[2] ?>%</span>
			</div>
		</div>		
		<div class="validators">
			<a href="https://webster.cs.washington.edu/validate-html.php"><img src="https://webster.cs.washington.edu/images/w3c-html.png" alt="Valid HTML5" /></a><br />
			<a href="https://webster.cs.washington.edu/validate-css.php"><img src="https://webster.cs.washington.edu/images/w3c-css.png" alt="Valid CSS" /></a>
		</div>
		<div class="rancbg">
			<img class="topbannerimg" src="https://webster.cs.washington.edu/images/rancidbanner.png" alt="Rancid Tomatoes" />
		</div>
	</body>
</html>
