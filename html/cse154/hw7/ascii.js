
var timer = null;
var index = 0;
var animationname = "Blank";
var speedvalue = 250;
var fontsize = 12;

window.onload = function(){
	
	var allParas = document.querySelectorAll("fieldset");
	for (var i = 0; i < allParas.length; i++) {
		allParas[i].style.display = "inline";
	}
	
	document.getElementById("animation").onchange = animation;
	document.getElementById("play").onclick = play;
	document.getElementById("stop").onclick = stop;
	document.getElementById("size").onchange = size;
	
	document.getElementById("stop").disabled = true;
	document.getElementById("fast").onchange= fast;
	document.getElementById("reg").onchange = reg;
	document.getElementById("slow").onchange = slow;
}

function speed(value) {
	speedvalue = value;
}

function fast() {
	speed(50);
}

function reg() {
	speed(250);
}
function slow() {
	speed(1500);
}

function animation() {
	var mylist = document.getElementById("animation");
	animationname = mylist.options[mylist.selectedIndex].value;
}

function animate() {
	// Clear the contents
	document.getElementById("animation").disabled = true`;
	document.getElementById("play").disabled = true;
	document.getElementById("stop").disabled = false;
	var mystring = ANIMATIONS[animationname];
	var myarray = mystring.split("=====\n");
	if (timer == null) {
		timer = setInterval(displayanimation, speedvalue,myarray);
	}
}

function displayanimation(myarray){
	
	if(index==myarray.length){
		clearInterval(timer);
		timer = null;
		index = 0;	
		animate();
	}else{
	
		document.getElementById("textbox").value = myarray[index];
		index = index+1;
	}
}

function play() {
	
	animate();
	
}

function stop() {
	clearInterval(timer);
	timer = null;
	index = 0;	
	document.getElementById("play").disabled = false;
	document.getElementById("stop").disabled = true;
	document.getElementById("animation").disabled = false;
}

function size() {
	var e = document.getElementById("size");
	var csize = e.options[e.selectedIndex].value;
	document.getElementById("myarea").style.fontSize=csize+"pt";
}