<!--
Jonathan Muller
11/7/2012
cse154-Autumn
Homework 6
Enjoy =D
-->

<?php
include("common.php");
?>

<!DOCTYPE html>
<html>
	<?php makeheader() ?>
	<body>
		<div id="frame">
			<?php makebanner() ?>

			<div id="main">
				<h1>The One Degree of Kevin Bacon</h1>
				<p>Type in an actor's name to see if he/she was ever in a movie with Kevin Bacon!</p>
				<p><img src="https://webster.cs.washington.edu/images/kevinbacon/kevin_bacon.jpg" alt="Kevin Bacon" /></p>

				<?php makesearches() ?>
			</div> <!-- end of #main div -->
		
			<?php makefooter() ?>
		</div> <!-- end of #frame div -->
	</body>
</html>
