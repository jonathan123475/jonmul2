<!--
Jonathan Muller
11/7/2012
cse154-Autumn
Homework 6
Enjoy =D
-->
<?php
$firstname = $_GET["firstname"];
$lastname = $_GET["lastname"];
include("common.php");
?>
<!DOCTYPE html>
<html>
	<?php makeheader() ?>
	<body>
		<div id="frame">
			<?php makebanner() ?>
			<div id="main">
				<?php
					# connect to the database
					$db = new PDO("mysql:dbname=imdb;host=localhost", "jonmul", "8gdYzAUJKmiu7");
		
					# this makes it print nice error messages if we make any mistakes in our query
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$first = $db->quote($firstname);
					$last = $db->quote($lastname);
					# do the SQL query to find the data we want
					$rows = $db->query("SELECT m.name, m.year FROM movies m JOIN roles r ON r.movie_id = m.id JOIN actors a ON a.id = r.actor_id WHERE a.last_name=$last AND a.first_name LIKE CONCAT('%',$first,'%') ORDER BY m.year DESC, m.name ;");
					$i = 0;
					#Checks to see if the actor has any movies
					if($rows->rowCount() > 0){
				?>
						<h1>Results for <?=$firstname." ".$lastname ?></h1>
						<table>
					<caption>All Films</caption>
					<tr><th>#</th><th>Title</th><th>Year</th></tr>
				<?php
					foreach ($rows as $row) {
						$i = $i+1;
						?>
							<tr><td vertical-align="left"><?=$i ?></td><td><?=$row[0] ?></td><td><?=$row[1] ?></td></tr>
						<?php
					}
					?>
					</table>
					<?php
					}else{ ?>
					<!-- actor not found in DB -->
					<h1>Actor <?=$firstname." ".$lastname ?> not found</h1>
					<?php } ?>
				<?php makesearches() ?>
			</div> <!-- end of #main div -->
			<?php makefooter() ?>
		</div> <!-- end of #frame div -->
	</body>
</html>
