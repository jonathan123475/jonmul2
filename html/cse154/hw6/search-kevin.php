<!--
Jonathan Muller
11/7/2012
cse154-Autumn
Homework 6
Enjoy =D
-->

<?php
$firstname = $_GET["firstname"];
$lastname = $_GET["lastname"];
include("common.php");
?>
<!DOCTYPE html>
<html>
	<?php makeheader() ?>
	<body>
		<div id="frame">
			<?php makebanner() ?>
			<div id="main">
				<?php
					# connect to the database
					$db = new PDO("mysql:dbname=imdb;host=localhost", "jonmul", "8gdYzAUJKmiu7");
					# this makes it print nice error messages if we make any mistakes in our query
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$first = $db->quote($firstname);
					$last = $db->quote($lastname);
					# do the SQL query to find the data we want
					$rows = $db->query("SELECT m.name, m.year FROM movies m JOIN roles r1 ON r1.movie_id = m.id JOIN actors input ON input.id = r1.actor_id JOIN roles r2 ON r2.movie_id = m.id JOIN actors bacon ON bacon.id = r2.actor_id WHERE input.last_name=$last AND bacon.id = 22591 AND input.first_name LIKE CONCAT('%',$first,'%') ORDER BY m.year DESC, m.name ;");
					$rows2 = $db->query("SELECT m.name, m.year FROM movies m JOIN roles r ON r.movie_id = m.id JOIN actors a ON a.id = r.actor_id WHERE a.last_name=$last AND a.first_name LIKE CONCAT('%',$first,'%') ORDER BY m.year DESC, m.name ;");
					$number = 0;
					#Checks to see if the actor has any movies
					if($rows->rowCount() > 0){
				?>
				<h1>Results for <?=$firstname." ".$lastname ?></h1>
				<table>
					<caption>Films with <?=$firstname." ".$lastname ?> and Kevin Bacon</caption>
					<tr><th>#</th><th>Title</th><th>Year</th></tr>
				<?php
					foreach ($rows as $row) {
						$number = $number+1;
						?>
					
						
							
							<tr><td vertical-align="left"><?=$number ?></td><td><?=$row[0] ?></td><td><?=$row[1] ?></td></tr>
							
			
						<?php 
					}
					?>
					</table>
					<?php
					}elseif($rows2->rowCount() > 0){ ?>
					<!-- Actor not found, but not in any of Kevin Bacon's movies -->
					<p><?=$firstname." ".$lastname ?> wasn't in any films with Kevin Bacon.</p>
					<?php }else{ ?>
					<!-- actor not found in DB -->
					<p>Actor <?=$firstname." ".$lastname ?> not found</p>
					<?php } ?>
				<?php makesearches() ?>
			</div> <!-- end of #main div -->
		
			<?php makefooter() ?>
		</div> <!-- end of #frame div -->
	</body>
</html>
