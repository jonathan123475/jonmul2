//Jonathan Muller
//12/9/2012
//cse154-Autumn
//Homework 9
//Enjoy =D

"use strict";

window.onload = function() {
	
	//Sends a request and creates the option list of names.
	var ajax = new XMLHttpRequest();
	ajax.onload = addnameoptions;
	ajax.onerror = errorFunctionName;
	ajax.open("GET", "https://webster.cs.washington.edu/cse154/babynames.php?type=list", true);
	ajax.send();
	
	//Search the different criteria when clicking search.
	document.getElementById('search').onclick = search;
};

function search(){
	
	document.getElementById("errors").innerHTML="";
	
	//Get the gender and name from the selected values
	var gender = getGender(document.getElementsByName("gender"));
	var dropdown = document.getElementById("allnames");
	var name = dropdown.options[dropdown.selectedIndex].value;
	
	//Search for and print out the meaning text for the given name
	searching("loadingmeaning", false);
	var ajax = new XMLHttpRequest();
	ajax.onload = searchmeaning;
	ajax.onerror = errorFunctionName;
	ajax.open("GET", "https://webster.cs.washington.edu/cse154/babynames.php?type=meaning&name="+name+"", true);
	ajax.send();
	
	//Search for and create the graph for the given name
	searching("loadinggraph", false);
	var ajax2 = new XMLHttpRequest();
	ajax2.onload = searchgraph;
	ajax2.onerror = errorFunctionName;
	ajax2.open("GET", "https://webster.cs.washington.edu/cse154/babynames.php?type=rank&name="+name+"&gender="+gender+"", true);
	ajax2.send();
	
	//Search for and create the celebs list for the given name
	searching("loadingcelebs", false);
	var ajax3 = new XMLHttpRequest();
	ajax3.onload = searchcelebs;
	ajax3.onerror = errorFunctionName;
	ajax3.open("GET", "https://webster.cs.washington.edu/cse154/babynames.php?type=celebs&name="+name+"&gender="+gender+"", true);
	ajax3.send();
	
}

//Returns the selected Gender
function getGender(obj){
	for(var i = 0; i < obj.length; i++) {
		if(obj[i].checked) {
			return obj[i].value;
		}
	}
}

//Print out the errors encountered into the errors ID field.
function seterrors(data){
	document.getElementById("errors").innerHTML += data + "\n";
}

function searchcelebs(){
	if (this.status == 200) {   // 200 means request succeeded
	
		var list = document.getElementById("celebs");
		list.innerHTML = "";
		var data = JSON.parse(this.responseText);
		//Create and append each celebs list element.
		for(var i=0;i<data.actors.length;i++){
			var bullet = document.createElement('li');
			
			bullet.innerHTML= data.actors[i].firstName +" "+ data.actors[i].lastName+" ("+data.actors[i].filmCount+" films)";
			list.appendChild(bullet);
		}
		searching("loadingcelebs", true);
	}else{
		seterrors("The celebs returned an error of " + this.status); 
	
	}
}
//toggles the 'searching' animation thingy.
function searching(myid, value){
	document.getElementById(myid).hidden=value;
}

//Creates the graph of the names
//	Essentially, it retrives and creates the cooresponding element tags and appends each
//tag to it's proper parent. Ultimately appending the Years and Ranks list.
function searchgraph(){
	document.getElementById("norankdata").style.display="none";
	var graph = document.getElementById('graph');
	graph.innerHTML="";
	if (this.status == 200) {   // 200 means request succeeded
		var response = this.responseXML;
		var yearlist = this.responseXML.getElementsByTagName("rank");
		
		
		var years = document.createElement('tr');
		var ranks = document.createElement('tr');
		
		for(var i=0;i<yearlist.length;i++){
			var rank = yearlist[i].firstChild.nodeValue;
			var year = yearlist[i].getAttribute("year");
			
			var mydiv = document.createElement('div');
			var mytd = document.createElement('td');
			var myth = document.createElement('th');
			myth.innerHTML = year;
			mydiv.innerHTML = rank;
			mydiv.setAttribute('id', "bar");
			mytd.appendChild(mydiv);
			var bheight= parseInt((1000-rank)/4);
			
			//Check the the high is 250 (which means that it's value is 0)
			if(bheight==250){
				bheight = 0;
			}else if(rank<=10){ 	//If the rank is less than or equal to 10, then the attribute is red.				
				mydiv.setAttribute('class', "popular"); //This discludes the case where rank = 0, because it is an else if statement 
			}
			mydiv.style.height= bheight+"px";
			years.appendChild(myth);
			ranks.appendChild(mytd);
			
		}
		//alert(yearlist.length);
		graph.appendChild(years);
		graph.appendChild(ranks);
		
		
	} else if(this.status==410) {
		document.getElementById("norankdata").style.display="block"; //Display the norankdata DIV if 410 error occurs.
	} else {
		seterrors("The graph returned an error of " + this.status); 
	
	}
	searching("loadinggraph", true);
	
}

//Retrieves and prints out the proper meaning text into the document.
function searchmeaning() {
  if (this.status == 200) {   // 200 means request succeeded
	document.getElementById("resultsarea").style.display="block";
    document.getElementById("meaning").innerHTML = this.responseText;
  } else {
    seterrors("The meaning returned an error of " + this.status); 
  }
  searching("loadingmeaning", true);
}

//Add the name options in the given list.
function addnameoptions() {
  if (this.status == 200) {   // 200 means request succeeded
    var entrystring = this.responseText; 
	var entry = entrystring.split("\n");
	var options =document.getElementById("allnames").innerHTML;
	for(var i=0; i<entry.length;i++){
		var list = document.getElementById('allnames');
		var newOp = document.createElement("option");
		newOp.text = entry[i];
		newOp.value = entry[i];
		list.appendChild(newOp);
	}
	
	
	searching("loadingnames", true);
  } else {
    seterrors("While adding the options, encountered error " + this.status); 
  }
  document.getElementById("allnames").disabled = false;
}

//Ajax errors encountered outside of the onload error returns this.
function errorFunctionName() {
	seterrors("While searching, encountered error #" + this.status); 
}